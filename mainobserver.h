#ifndef MAINOBSERVER_H
#define MAINOBSERVER_H

#include "mainwindow.h"

class MainWindow;

class MainObserver : public Observer
{
public:
    MainObserver(MainWindow *m);

    void update(int ID);

private:
    MainWindow *mainWindow;
};

#endif // MAINOBSERVER_H
