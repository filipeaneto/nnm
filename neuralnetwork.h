#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#include "arffdata.h"
#include <QString>
#include <exception>
#include <QTextBrowser>

class NetworkException : public std::exception
{
public:
    NetworkException(const char *m) : message(m) {}
    const char *what() const throw () { return message; }

private:
    const char *message;
};

class NeuralNetwork
{

public:
    // Estes dois metodos devem ser chamados antes de executar a rede
    virtual void setClass(const QString &classAtt) = 0;
    virtual void setArff(const ArffData *arff) = 0;
    virtual void setArgs(const QString &args) = 0;

    virtual void run() throw (NetworkException) = 0;

    // Apos executar a rede, estes dois metodos devem ser chamados
    virtual QTextBrowser *popTextBrowser() throw (NetworkException) = 0;
    virtual QWidget *popWidget() throw (NetworkException) = 0;

    virtual const QString defaultArgs() = 0;
    virtual const QString name() = 0;

    virtual int arffTypeExpected() const = 0;
};

#endif // NEURALNETWORK_H
