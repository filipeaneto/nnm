#include "mainobserver.h"

MainObserver::MainObserver(MainWindow *m) :
    mainWindow(m)
{

}

void MainObserver::update(int ID)
{
    if(ID & Notification::ResultModification)
    {
        mainWindow->updateResultWidget();
    }
    else if(ID & Notification::StatusModification)
    {
        mainWindow->showMessage();
    }
}
