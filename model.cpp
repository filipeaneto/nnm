#include <model.h>

Model::Model() :
    arff(0), notificationID(Notification::None)
{

}

void Model::addObserver(Observer *observer)
{
    observers.push_back(observer);
}

void Model::notify()
{
    for(QList<Observer*>::iterator it = observers.begin();
        it != observers.end(); it++)
    {
        (*it)->update(notificationID);
    }

    notificationID = Notification::None;
}

void Model::setArff(const QString &filename)
{
    ArffData *tmp;

    if(arff != 0)
        tmp = arff;

    try
    {
        if(filename.contains(".arff"))
        {
            arff = new ArffData(filename);
            notificationID += Notification::ArffModification;
        }
        else if(filename.contains(".pat"))
        {
            arff = new Pattern(filename);
            notificationID += Notification::PatternModification;
        }

        feedStatus("Relation " + arff->getRelation() + " was successful opened.");
        notificationID += Notification::StatusModification;
    }
    catch(ArffException e)
    {
        arff = tmp;
        feedStatus(QString("Arff file couldn't be opened: ") + e.what() + ".");
        notificationID += Notification::StatusModification;
    }

    notify();
}

const ArffData * Model::getArff() const
{
    return arff;
}

void Model::removeArffAtt(int i)
{
    if(arff == 0)
        return;

    arff->removeAttribute(i);
    notificationID += Notification::ArffModification;

    notify();
}

void Model::feedStatus(const QString &status)
{
    statusList.push_back(status);
    notificationID += Notification::StatusModification;
    notify();
}

void Model::setResultWidget(QWidget *widget)
{
    result = widget;
    notificationID += Notification::ResultModification;

    notify();
}

QWidget * Model::getResultWidget() const
{
    return result;
}

QStringList Model::consumeStatus()
{
    QStringList list(statusList);
    statusList.clear();
    return list;
}
