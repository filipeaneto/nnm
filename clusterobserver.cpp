#include "clusterobserver.h"

ClusterObserver::ClusterObserver(Cluster *c) :
    cluster(c)
{

}

void ClusterObserver::update(int ID)
{
    if(ID & Notification::ArffModification)
    {
        cluster->setEnabled(true);
        cluster->setArff();
    }
    else if(ID & Notification::PatternModification)
    {
        cluster->setEnabled(false);
    }
}
