#-------------------------------------------------
#
# Project created by QtCreator 2012-04-19T16:28:06
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = NNM
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    preprocess.cpp \
    cluster.cpp \
    utils.cpp \
    arffdata.cpp \
    model.cpp \
    preprocessobserver.cpp \
    som.cpp \
    clusterobserver.cpp \
    mainobserver.cpp \
    featuremap.cpp \
    classify.cpp \
    classifyobserver.cpp \
    hopfield.cpp \
    pattern.cpp \
    patternview.cpp

HEADERS  += mainwindow.h \
    preprocess.h \
    cluster.h \
    utils.h \
    arffdata.h \
    model.h \
    observer.h \
    preprocessobserver.h \
    subject.h \
    som.h \
    neuralnetwork.h \
    clusterobserver.h \
    mainobserver.h \
    featuremap.h \
    classify.h \
    classifyobserver.h \
    hopfield.h \
    pattern.h \
    patternview.h

FORMS    += mainwindow.ui \
    preprocess.ui \
    cluster.ui \
    featuremap.ui \
    classify.ui \
    patternview.ui








