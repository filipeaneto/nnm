#include "utils.h"

QString Utils::getNow()
{
    return QString("[") + QDateTime::currentDateTime().toString() + "]";
}

QTextBrowser &Utils::merge(const QTextBrowser &t1, const QTextBrowser &t2)
{
    QTextBrowser result;

    result.setPlainText(t1.toPlainText() + t2.toPlainText());
}

