#include "arffdata.h"

ArffData::ArffData(QString fileName) throw (ArffException)
{
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file);

    bool header = true;

    while(!stream.atEnd()){
        QString line = stream.readLine();

        if(line.isEmpty() || line.startsWith("%"))
            continue;

        header = header ? readHeaderLine(line) : readDataLine(line);
    }
}

ArffData::ArffData() throw (ArffException)
{

}

ArffData::ArffData(const QList<ArffData *> &sources) throw (ArffException)
{
    throw ArffException("Not implemented yet.");
}

ArffData::~ArffData()
{

}

const QString &ArffData::getRelation() const
{
    return relation;
}

bool ArffData::readHeaderLine(const QString &line) throw (ArffException)
{
    if( line.startsWith("@data", Qt::CaseInsensitive) )
    {
        return false;
    }
    else if( line.startsWith("@attribute", Qt::CaseInsensitive) )
    {
        //if(line.contains(".") || line.contains("-"))
        //    throw ArffException("Invalid nominal type!");

        QString corrected(line);
        corrected.replace("{"," ");
        corrected.replace("}"," ");
        corrected.replace(","," ");

        // @attribute attName attType
        const QStringList list = corrected.split(QRegExp("\\s+"), QString::SkipEmptyParts); // separa todas as palavras

        if(list[2].compare("real", Qt::CaseInsensitive) == 0 ||
                list[2].compare("integer", Qt::CaseInsensitive) == 0)
        {
            atts.push_back(Attribute(list[1], list[2]));
        }
        else if(list.size() > 3)
        {
            QStringList type;
            type.push_back("nominal");
            for(int i = 2; i < list.size(); i++)
            {
                type.push_back(list[i]);
            }

            atts.push_back(Attribute(list[1], type));
        }

        else
        {
            throw ArffException("Attribute not supported");
        }
    }
    else if(line.startsWith("@relation", Qt::CaseInsensitive) )
    {
        // @relation relationName
        relation = line.split(QRegExp("\\s+"))[1]; // separa por espaco
    }


    return true;
}

bool ArffData::readDataLine(const QString &line) throw (ArffException)
{
    QStringList values = line.split(",", QString::SkipEmptyParts);

    if(values.size() != atts.size())
    {
        throw ArffException("Wrong input data size");
    }

    data.push_back(InputVector(atts, values));

    return false;
}

InputVector::InputVector(const QList<Attribute> &t, const QStringList &v) throw (ArffException)
{
    if(v.size() != t.size())
        throw ArffException("Wrong input data size");

    for(int i = 0; i < t.size(); i++)
    {
        dataType.push_back(t[i].type[0]);

        if(t[i].type[0].compare("real", Qt::CaseInsensitive) == 0)
        {
            inputs.push_back(InputData(v[i].toDouble()));
        }
        else if(t[i].type[0].compare("integer", Qt::CaseInsensitive) == 0)
        {
            inputs.push_back(InputData(v[i].toInt()));
        }
        else if(t[i].type[0].compare("nominal", Qt::CaseInsensitive) == 0)
        {
            inputs.push_back(InputData(v[i].toStdString().c_str()));
        }
    }
}

InputVector::InputVector(const InputVector &source)
{
    dataType = source.dataType;
    inputs = source.inputs;
}

const InputVector &InputVector::operator=(const InputVector &source)
{
    dataType = source.dataType;
    inputs = source.inputs;
    return *this;
}

InputVector::~InputVector()
{

}

InputData::InputData()
{

}

InputData::InputData(double value, const QString &t) :
    dValue(value), type(t)
{
}

InputData::InputData(int value, const QString &t) :
    iValue(value), type(t)
{

}

InputData::InputData(const QString &value, const QString &t) :
    sValue(value), type(t)
{

}

InputData::InputData(const InputData &source)
{
    *this = source;
}

InputData::~InputData()
{

}

const InputData &InputData::operator=(const InputData &source)
{
    this->type = source.type;

    if(source.type.compare("real", Qt::CaseInsensitive) == 0)
    {
        this->dValue = source.dValue;
    }
    else if(source.type.compare("integer", Qt::CaseInsensitive) == 0)
    {
        this->iValue = source.iValue;
    }
    else if(source.type.compare("nominal", Qt::CaseInsensitive) == 0)
    {
        this->sValue = source.sValue;
    }

    return *this;
}

void InputData::setValue(double value) throw (ArffException)
{
    if(type.compare("real", Qt::CaseInsensitive) == 0)
        dValue = value;
    else throw ArffException("Wrong data type");
}

void InputData::setValue(int value) throw (ArffException)
{
    if(type.compare("integer", Qt::CaseInsensitive) == 0)
        iValue = value;
    else throw ArffException("Wrong data type");
}

void InputData::setValue(const QString &value) throw (ArffException)
{
    if(type.compare("nominal", Qt::CaseInsensitive) == 0)
        sValue = value;
    else throw ArffException("Wrong data type");
}

double InputData::getDValue() const throw (ArffException)
{
    if(type.compare("real", Qt::CaseInsensitive) == 0)
        return dValue;
    else throw ArffException("Wrong data type");
}
int InputData::getIValue() const throw (ArffException)
{
    if(type.compare("integer", Qt::CaseInsensitive) == 0)
        return iValue;
    else throw ArffException("Wrong data type");
}
const QString &InputData::getSValue() const throw (ArffException)
{
    if(type.compare("nominal", Qt::CaseInsensitive) == 0)
        return sValue;
    else throw ArffException("Wrong data type");
}

const QString &InputData::getType() const
{
    return type;
}

int ArffData::getNAttributes() const
{
    return atts.size();
}

int ArffData::getNInstances() const
{
    return data.size();
}

const QString & ArffData::getAttName(int i) const
{
    return atts.at(i).name;
}

const QString & ArffData::getAttType(int i) const
{
    return atts.at(i).type[0];
}

void ArffData::removeAttribute(int i)
{
    QList<Attribute>::iterator it = atts.begin();
    for(int j = 0; j < i; j++) it++;
    atts.erase(it);

    for(QList<InputVector>::iterator it = data.begin(); it != data.end(); it++)
    {
        //printf("para cada dado remover a coluna!\n");
        (*it).removeAttribute(i);
    }
}

void InputVector::removeAttribute(int i)
{
    QStringList::iterator it1 = dataType.begin();
    for(int j = 0; j < i; j++) it1++;
    dataType.erase(it1);

    QList<InputData>::iterator it2 = inputs.begin();
    for(int j = 0; j < i; j++) it2++;
    inputs.erase(it2);


}

const InputData &ArffData::at(int i, int j) const
{
    return data.at(i).get(j);
}

const InputData &InputVector::get(int i) const
{
    return inputs[i];
}

int ArffData::countNumericAttributes() const
{
    int count = 0;

    for(int i = 0; i < getNAttributes(); i++)
    {
        if(atts.at(i).type.at(0).compare("real", Qt::CaseInsensitive) == 0 ||
           atts.at(i).type.at(0).compare("integer", Qt::CaseInsensitive) == 0)
        {
            count++;
        }
    }

    return count;
}

QStringList ArffData::classesFrom(const QString &attributeName) const
{
    for(QList<Attribute>::const_iterator it = atts.begin(); it != atts.end(); it++)
    {
        if((*it).name.compare(attributeName, Qt::CaseInsensitive) == 0)
        {
            return (*it).type.mid(1);
        }
    }
    return QStringList();
}

const InputData &ArffData::attAt(int i, const QString &attName) const
{
    int j = 0;
    for(QList<Attribute>::const_iterator it = atts.begin(); it != atts.end(); it++, j++)
    {
        if((*it).name.compare(attName) == 0)
        {
            break;
        }
    }

    return data.at(i).get(j);
}

void ArffData::split(float percent, ArffData *a1, ArffData *a2) const throw (ArffException)
{
    float flag = percent <= 100.0 - percent ? percent : 100.0 - percent;
    flag *= data.size();
    if(flag < 1)
    {
        throw ArffException("Cannot split the arff");
    }

    QList<InputVector> a1Data;
    QList<InputVector> a2Data;

    srand(time(0));
    int insert;
    bool a1IsFull = false;
    bool a2IsFull = false;

    for(int i = 0; i < data.size(); i++)
    {
        insert = rand() % 100;
        if(a2IsFull || (insert <= percent && !a1IsFull))
        {
            a1Data.push_back(data.at(i));
            if(a1Data.size() >= (percent * data.size()))
            {
                a1IsFull = true;
            }
        }
        else
        {
            a2Data.push_back(data.at(i));
            if(a2Data.size() >= ((100.0 - percent) * data.size()))
            {
                a2IsFull = true;
            }
        }
    }

    a1->relation = relation;
    a1->data = a1Data;
    a1->atts = atts;

    a2->relation = relation;
    a2->data = a2Data;
    a2->atts = atts;
}

void ArffData::split(int fold, QList<ArffData *> &arffs) const throw (ArffException)
{
    if(fold < data.size())
    {
        throw ArffException("Cannot split the arff");
    }

    QVector<QList<InputVector> > v(fold);
    QList<InputVector> cpData = data;

    srand(time(0));
    int random;
    bool flag = true;

    while(flag)
    {
        for(int i = 0; i < fold && flag; i++)
        {
            random = rand() % cpData.size();
            v[i].push_back(cpData.at(random));
            cpData.removeAt(random);

            if(cpData.isEmpty())
            {
                flag = false;
            }
        }
    }

    for(int i = 0; i < fold; i++)
    {
        ArffData *arff = new ArffData();
        arff->relation = relation;
        arff->data = v.at(i);
        arff->atts = atts;
        arffs.push_back(arff);
    }
}
