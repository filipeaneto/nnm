#include "hopfield.h"

Hopfield::Hopfield() :
    textBrowser(0), patternView(0), netDefaultArgs("width=32 height=32 threshold_all=0"), netName("Hopfield"),
    weight(0), threshold(0), pInputs(0), inputs(0)
{
}

void Hopfield::setClass(const QString &classAtt)
{
    this->patternAtt = classAtt;
}

void Hopfield::setArgs(const QString &args)
{
    this->args = args;
}

void Hopfield::setArff(const ArffData *arff)
{
    this->arff = arff;
}

QTextBrowser *Hopfield::popTextBrowser() throw (NetworkException)
{
    if(textBrowser == 0)
        throw NetworkException("You must run the network first");

    // Reinicializa o textBrowser
    QTextBrowser *tmp = textBrowser;
    textBrowser = 0;

    // Se o featureMap ja foi retirado, limpa a memoria
    if(patternView == 0)
        clear();

    return tmp;
}

QWidget *Hopfield::popWidget() throw (NetworkException)
{
    if(patternView == 0)
        throw NetworkException("You must run the network first");

    // Reinicializa o featureMap
    QWidget *tmp = patternView;
    patternView = 0;

    // Se o textBrowser ja foi retirado, limpa a memoria
    if(textBrowser == 0)
        clear();

    return tmp;
}

void Hopfield::run() throw (NetworkException)
{
    // Caso ainda haja algo pra ser limpo, limpe
    if(patternView != 0 || textBrowser != 0){
        if(patternView != 0)
            delete patternView;
        if(textBrowser != 0)
            delete textBrowser;
        clear();
    }

    if(arff == 0 || patternAtt.isEmpty())
        throw NetworkException("Missing arff or pattern class");

    // Inicializa matrix de memoria dos padroes
    init();

    // Aprendizado Hebbiano
    for(int i = 0; i < nNeurons; i++)
    {
        for(int j = 0; j < nNeurons; j++)
        {

            weight[i][j] = 0;
            if(i == j)
            {
                continue;
            }

            for(int k = 0; k < patterns.size(); k++)
            {
                weight[i][j] += pInputs[k][i]*pInputs[k][j];
            }
            weight[i][j] /= nNeurons;
        }
    }

    // Preenche o widget de padroes
    fillWidget();
}

const QString Hopfield::defaultArgs()
{
    return netDefaultArgs;
}

const QString Hopfield::name()
{
    return netName;
}

int Hopfield::arffTypeExpected() const
{
    return 2;
}

void Hopfield::readArgs() throw (NetworkException)
{
    textBrowser->append("Arguments: " + args);

    QStringList commands = args.split(QRegExp("\\s+"), QString::SkipEmptyParts);

    width = height = 0;

    bool t = true;

    for(QStringList::Iterator it = commands.begin(); it != commands.end(); it++)
    {
        if((*it).startsWith("height"))
        {
            height = (*it).split("=")[1].toInt();
        }
        else if((*it).startsWith("width"))
        {
            width = (*it).split("=")[1].toInt();
        }
        else if((*it).startsWith("threshold_all"))
        {
            if(t)
                t = false;
            else
                throw NetworkException("Wrong arguments");

            threshold[0] = (*it).split("=")[1].toInt();
            for(int i = 1; i < nNeurons; i++)
            {
                threshold[i] = threshold[0];
            }
        }
        else if((*it).startsWith("threshold_vector"))
        {
            if(t)
                t = false;
            else
                throw NetworkException("Wrong arguments");

            QStringList thr = (*it).split("=");
            thr = thr.at(1).split(",");

            if(thr.size() != nNeurons)
            {
                throw NetworkException("Wrong size of arguments");
            }

            for(int i = 0; i < nNeurons; i++)
            {
                threshold[i] = thr.at(i).toDouble();
            }
        }
        else
        {
            textBrowser->clear();
            QString message("Unknown command ");
            message += (*it);
            throw NetworkException(message.toStdString().c_str());
        }
    }

    if(width * height != nNeurons)
    {
        textBrowser->clear();
        throw NetworkException("Wrong size argument");
    }

    if(t)
    {
        throw NetworkException("Missing threshold");
    }
}

void Hopfield::readArff() throw (NetworkException)
{
    textBrowser->append("Relation: " + arff->getRelation());
    textBrowser->append("Instances: " + QString().setNum(arff->getNInstances()));
    textBrowser->append("Attributes: " + QString().setNum(arff->getNAttributes()));

    patterns = arff->classesFrom(patternAtt);
    nInputs = arff->getNInstances() - patterns.size();
    nNeurons = arff->countNumericAttributes();

    if(nNeurons < 2)
    {
        textBrowser->clear();
        throw NetworkException("Few numeric attributes");
    }

    pInputs = new int*[patterns.size()];
    for(int i = 0; i < patterns.size(); i++)
        pInputs[i] = new int[nNeurons];

    inputs = new int*[nInputs];
    for(int i = 0; i < nInputs; i++)
        inputs[i] = new int[nNeurons];

    for(int i = 0; i < patterns.size(); i++)
    {
        for(int j = 0; j < nNeurons; j++)
        {
            const InputData data = arff->at(i,j);
            if(data.getType().compare("integer", Qt::CaseInsensitive) != 0)
                throw NetworkException("Internal pattern file error");

            pInputs[i][j] = data.getIValue() == 0 ? -1 : data.getIValue();
            //printf("%3d ", pInputs[i][j]);
        }
        //printf("\n");
    }

    for(int i = patterns.size(), k = 0; i < nInputs + patterns.size(); i++, k++)
    {
        for(int j = 0; j < nNeurons; j++)
        {
            const InputData data = arff->at(i,j);
            if(data.getType().compare("integer", Qt::CaseInsensitive) != 0)
                throw NetworkException("Internal pattern file error");

            inputs[k][j] = data.getIValue() == 0 ? -1 : data.getIValue();
        }
    }

    threshold = new double[nNeurons];
}

void Hopfield::init() throw (NetworkException)
{
    textBrowser = new QTextBrowser();

    QString line = "<span style = 'color: red'>Run Information</span><br>";
    textBrowser->append(line);

    try
    {
        readArff();
        readArgs();
    }
    catch(NetworkException &e)
    {
        delete textBrowser;
        textBrowser = 0;
        throw e;
    }

    weight = new double*[nNeurons];
    for(int i = 0; i < nNeurons; i++)
        weight[i] = new double[nNeurons];
}

void Hopfield::clear()
{
    if(weight != 0)
    {
        for(int i = 0; i < nNeurons; i++)
            delete[] weight[i];
        delete[] weight;
        weight = 0;
    }

    if(inputs != 0)
    {
        for(int i = 0; i < nInputs; i++)
            delete[] inputs[i];
        delete[] inputs;
        inputs = 0;
    }

    if(pInputs != 0)
    {
        for(int i = 0; i < patterns.size(); i++)
            delete[] pInputs[i];
        delete[] pInputs;
        pInputs = 0;
    }

    if(threshold != 0)
    {
        delete[] threshold;
        threshold = 0;
    }
}

void Hopfield::fillWidget()
{
    // cria o Pattern View
    patternView = new PatternView(width, height);
    patternView->setWeightMatrix(weight);
    patternView->setInputs(inputs, nInputs);
    patternView->setPatterns(pInputs, patterns);
    patternView->setThreshold(threshold);
    patternView->init();

    // remove os dados do interior da rede
    weight = 0;
    inputs = 0;
    pInputs = 0;
    threshold = 0;
}
