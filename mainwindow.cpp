#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    lastResult(0)
{
    ui->setupUi(this);

    model = new Model();

    preprocess = new Preprocess(model, this);
    cluster = new Cluster(model, this);
    classify = new Classify(model, this);

    model->addObserver(new PreprocessObserver(preprocess));
    model->addObserver(new ClusterObserver(cluster));
    model->addObserver(new ClassifyObserver(classify));
    model->addObserver(new MainObserver(this));

    ui->tab_Preprocess->layout()->addWidget(preprocess);
    ui->tab_Cluster->layout()->addWidget(cluster);
    ui->tab_Classify->layout()->addWidget(classify);

    ui->textBrowser->insertPlainText(Utils::getNow() + " NNM was started correctly");

    ui->mainToolBar->setVisible(false);
    ui->menuBar->setVisible(false);
    ui->statusBar->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateResultWidget()
{
    if(lastResult != 0)
    {
        ui->tab_Result->layout()->removeWidget(lastResult);
        lastResult->setVisible(false);
    }

    lastResult = model->getResultWidget();
    lastResult->setVisible(true);
    ui->tab_Result->layout()->addWidget(lastResult);
}

void MainWindow::showMessage()
{
    QStringList status = model->consumeStatus();
    for(QStringList::const_iterator it = status.begin(); it != status.end(); it++)
    {
        ui->textBrowser->append(Utils::getNow() + " " + (*it));
        ui->label_Status->setText((*it));
    }

}
