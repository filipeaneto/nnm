#ifndef KOHONENMAP_H
#define KOHONENMAP_H

#include <QWidget>
#include <QTableWidget>

namespace Ui {
class FeatureMap;
}

class FeatureMap : public QWidget
{
    Q_OBJECT
    
public:
    explicit FeatureMap(QWidget *parent = 0);
    ~FeatureMap();

    QTableWidget *getMapTable();
    QTableWidget *getClassTable();
    QTableWidget *getWeightTable();

    void disableMap();
    
private:
    Ui::FeatureMap *ui;
};

#endif // KOHONENMAP_H
