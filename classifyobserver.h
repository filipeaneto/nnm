#ifndef CLASSIFYOBSERVER_H
#define CLASSIFYOBSERVER_H

#include <observer.h>
#include <classify.h>

class ClassifyObserver : public Observer
{
public:
    ClassifyObserver(Classify *c);

    void update(int ID);

private:
    Classify *classify;
};

#endif // CLASSIFYOBSERVER_H
