#ifndef CLUSTEROBSERVER_H
#define CLUSTEROBSERVER_H

#include <observer.h>
#include <cluster.h>

class ClusterObserver : public Observer
{
public:
    ClusterObserver(Cluster *c);

    void update(int ID);

private:
    Cluster *cluster;
};

#endif // CLUSTEROBSERVER_H
