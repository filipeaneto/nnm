#include "cluster.h"
#include "ui_cluster.h"

Cluster::Cluster(Model *m, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Cluster),
    model(m)
{
    ui->setupUi(this);
    //setEnabled(false);
    ui->pushButton_Start->setEnabled(false);

    nets.push_back(new SOM());

    for(int i = 0; i < nets.size(); i++)
    {
        ui->comboBox_Nets->addItem(nets[i]->name());
    }

    lastTextBrowser = ui->textBrowser;
}

Cluster::~Cluster()
{
    delete ui;
}

void Cluster::on_radioButton_PercentageSplit_toggled(bool checked)
{
    if(checked) {
        ui->spinBox_PercentageSplit->setEnabled(true);
    } else {
        ui->spinBox_PercentageSplit->setEnabled(false);
    }
}

void Cluster::setArff()
{
    ui->comboBox_Class->clear();

    for(int i = 0; i < nets.size(); i++)
    {
        nets[i]->setArff(model->getArff());
    }

    for(int i = 0; i < model->getArff()->getNAttributes(); i++)
    {
        QString type = model->getArff()->getAttType(i);
        if(type.compare("nominal", Qt::CaseInsensitive) == 0)
        {
            ui->comboBox_Class->addItem(model->getArff()->getAttName(i));
        }
    }
    ui->comboBox_Class->addItem("");
}

void Cluster::on_pushButton_Start_clicked()
{
    ui->pushButton_Start->setEnabled(false);

    runNetwork();

    ui->pushButton_Start->setEnabled(true);
}

void Cluster::on_comboBox_Nets_currentIndexChanged(int index)
{
    ui->lineEdit_Args->setText(nets[index]->defaultArgs());
}

void Cluster::runNetwork()
{
    NeuralNetwork *net = nets[ui->comboBox_Nets->currentIndex()];
    net->setArgs(ui->lineEdit_Args->text());

    ui->textBrowser->clear();

    try
    {
        net->setClass(ui->comboBox_Class->currentText());

        model->feedStatus(net->name() + " was started using " + model->getArff()->getRelation() + " relation.");

        net->run();

        model->feedStatus(net->name() + " was successful executed.");

        ui->comboBox_Results->addItem(QString().setNum(resultTabs.size()+1) + " - " + net->name()
                                      + " (" + model->getArff()->getRelation() + ")");
        ui->comboBox_Results->setCurrentIndex(resultTabs.size());

        QWidget *widget = net->popWidget();
        QTextBrowser *tb = net->popTextBrowser();

        resultTabs.push_back(widget);
        resultText.push_back(tb);

        model->setResultWidget(widget);
        ui->groupBox_Output->layout()->addWidget(tb);
        lastTextBrowser->setVisible(false);
        lastTextBrowser = tb;
    }
    catch(NetworkException e)
    {
        model->feedStatus(net->name() + " couldn't be started: " + e.what() + ".");
    }
}

void Cluster::on_comboBox_Results_currentIndexChanged(int index)
{
    if(index >= resultTabs.size())
        return;

    model->setResultWidget(resultTabs.at(index));
    resultText.at(index)->setVisible(true);
    lastTextBrowser->setVisible(false);
    lastTextBrowser = resultText.at(index);
}

void Cluster::setEnabled(bool flag) {
    ui->pushButton_Start->setEnabled(flag);
}
