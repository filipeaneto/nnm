#include "featuremap.h"
#include "ui_featuremap.h"

FeatureMap::FeatureMap(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FeatureMap)
{
    ui->setupUi(this);
}

FeatureMap::~FeatureMap()
{
    delete ui;
}

QTableWidget *FeatureMap::getWeightTable()
{
    return ui->tableWidget_WeightMatrix;
}

void FeatureMap::disableMap()
{
    ui->tabWidget_Map->removeTab(0);
}

QTableWidget *FeatureMap::getClassTable()
{
    return ui->tableWidget_Class;
}

QTableWidget *FeatureMap::getMapTable()
{
    return ui->tableWidget_FeatureMap;
}
