#include "som.h"

SOM::SOM() :
    textBrowser(0), featureMap(0), arff(0), netName("SOM")
{
    netDefaultArgs += "height=10 width=10 phase=ordering ilr=2.5 ";
    netDefaultArgs += "lrk=100 iew=14.4 ewk=140 epochs=100 phase=convergence ";
    netDefaultArgs += "ilr=1.4 lrk=100 iew=5 ewk=100 epochs=1000";
}

SOM::~SOM()
{

}

void SOM::setClass(const QString &classAtt)
{
    this->classAtt = classAtt;
}

void SOM::setArgs(const QString &args)
{
    this->args = args;
}

void SOM::setArff(const ArffData *arff)
{
    this->arff = arff;
}

QTextBrowser *SOM::popTextBrowser() throw (NetworkException)
{
    if(textBrowser == 0)
        throw NetworkException("You must run the network first");
        
    // Reinicializa o textBrowser
    QTextBrowser *tmp = textBrowser;
    textBrowser = 0;
    
    // Se o featureMap ja foi retirado, limpa a memoria
    if(featureMap == 0)
        clear();
    
    return tmp;
}

QWidget *SOM::popWidget() throw (NetworkException)
{
    if(featureMap == 0)
        throw NetworkException("You must run the network first");
    
    // Reinicializa o featureMap
    QWidget *tmp = featureMap;
    featureMap = 0;
    
    // Se o textBrowser ja foi retirado, limpa a memoria
    if(textBrowser == 0)
        clear();
        
    return tmp;
}

void SOM::run() throw (NetworkException)
{
    // Caso ainda haja algo pra ser limpo, limpe
    if(featureMap != 0 || textBrowser != 0){
        if(featureMap != 0)
            delete featureMap;
        if(textBrowser != 0)
            delete textBrowser;
        clear();
    }

    if(arff == 0 || args.isEmpty())
        throw NetworkException("Missing arff or arguments");

    // Inicializa matrix de pesos e entradas
    init();

    // Roda o algoritmo de treinamento da rede
    // Para cada fase
    for(phase = OrderingPhase; phase <= ConvergencePhase; phase++)
    {
        // Roda nEpochs vezes todo o conjunto de treinamento
        for(int e = 0; e < nEpochs[phase]; e++)
        {
            double learningRate = n(e);

            // Atualizando pesos pela formula de atualizacao
            // Wij(k+1) = Wij(k) + n(k)*Hwi(k)*( Xnj = Wij(k) )
            for(int k = 0; k < nInputs; k++)
            {
                int winner = getWinner(k);

                for(int i = 0; i < nClusters; i++)
                {
                    for(int j = 0; j < nAttributes; j++)
                    {
                        weight[i][j] = weight[i][j] + 
                                       learningRate * h(winner, i, e) * 
                                       (inputs[k][j] - weight[i][j]);
                    }
                }
            }
        }
    }

    // Preenche a matrix de pesos
    // Preenche o mapa de caracteristicas
    fillWidget();
}

const QString SOM::defaultArgs()
{
    return netDefaultArgs;
}

const QString SOM::name()
{
    return netName;
}

int SOM::arffTypeExpected() const
{
    return 3;
}

void SOM::readArgs() throw (NetworkException)
{
    textBrowser->append("Arguments: " + args);

    QStringList commands = args.split(QRegExp("\\s+"), QString::SkipEmptyParts);

    height = width = 0;
    phase = OrderingPhase;

    for(int i = 0; i < 2; i++)
    {
        initialLearningRate[i] = 0;
        learningRateK[i] = 0;
        initialEffectiveWidth[i] = 0;
        effectiveWidthK[i] = 0;
        nEpochs[i] = 0;
    }

    for(QStringList::Iterator it = commands.begin(); it != commands.end(); it++)
    {
        if((*it).startsWith("height"))
        {
            height = (*it).split("=")[1].toInt();
        }
        else if((*it).startsWith("width"))
        {
            width = (*it).split("=")[1].toInt();
        }
        else if((*it).startsWith("phase"))
        {
            QString sPhase = (*it).split("=")[1];
            if(sPhase.compare("ordering") == 0)
            {
                phase = OrderingPhase;
            }
            else if(sPhase.compare("convergence") == 0)
            {
                phase = ConvergencePhase;
            }
            else throw NetworkException("Unknown phase");
        }
        else if((*it).startsWith("ilr"))
        {
             initialLearningRate[phase] = (*it).split("=")[1].toDouble();
        }
        else if((*it).startsWith("lrk"))
        {
             learningRateK[phase] = (*it).split("=")[1].toDouble();
        }
        else if((*it).startsWith("iew"))
        {
             initialEffectiveWidth[phase] = (*it).split("=")[1].toDouble();
        }
        else if((*it).startsWith("ewk"))
        {
             effectiveWidthK[phase] = (*it).split("=")[1].toDouble();
        }
        else if((*it).startsWith("epochs"))
        {
             nEpochs[phase] = (*it).split("=")[1].toDouble();
        }
        else
        {
            textBrowser->clear();
            QString message("Unknown command ");
            message += (*it);
            throw NetworkException(message.toStdString().c_str());
        }
    }

    nClusters = height * width;
    if(nClusters < 2 || height < 1 || width < 1)
    {
        textBrowser->clear();
        throw NetworkException("Wrong number of clusters");
    }
}

void SOM::readArff() throw (NetworkException)
{

    textBrowser->append("Relation: " + arff->getRelation());
    textBrowser->append("Instances: " + QString().setNum(arff->getNInstances()));
    textBrowser->append("Attributes: " + QString().setNum(arff->getNAttributes()));

    nInputs = arff->getNInstances();
    nAttributes = arff->countNumericAttributes();

    if(nAttributes < 2)
    {
        textBrowser->clear();
        throw NetworkException("Few numeric attributes");
    }

    inputs = new double*[nInputs];
    for(int i = 0; i < nInputs; i++)
        inputs[i] = new double[nAttributes];


    for(int i = 0; i < nInputs; i++)
    {
        for(int j = 0, k = 0; j < nAttributes; k++)
        {
            const InputData data = arff->at(i, k);
            if(data.getType().compare("nominal", Qt::CaseInsensitive) == 0)
            {
                continue;
            }
            else if(data.getType().compare("real", Qt::CaseInsensitive) == 0)
            {
                inputs[i][j] = data.getDValue();
            }
            else if (data.getType().compare("integer", Qt::CaseInsensitive) == 0)
            {
                inputs[i][j] = (double) data.getIValue();
            }

            j++;
        }
    }
}

void SOM::fillWidget()
{
    fillWeightMatrix();
    fillFeatureMap();
}

void SOM::fillWeightMatrix()
{
    QTableWidget *weightMatrix = featureMap->getWeightTable();

    weightMatrix->setRowCount(nClusters);
    weightMatrix->setColumnCount(nAttributes);

    for(int i = 0; i < nClusters; i++)
    {
        for(int j = 0; j < nAttributes; j++)
        {
            weightMatrix->setItem(i, j, new QTableWidgetItem(QString().setNum(weight[i][j])));
        }
    }
}

void SOM::fillFeatureMap()
{
    if(classAtt.isEmpty())
    {
        featureMap->disableMap();
        return;
    }

    QTableWidget *tableClass  = featureMap->getClassTable();
    QTableWidget *tableMap    = featureMap->getMapTable();

    QStringList classes = arff->classesFrom(classAtt);

    int **clusters = new int*[classes.size()];

    for(int i = 0; i < classes.size(); i++)
    {
        clusters[i] = new int[nClusters];
        for(int j = 0; j < nClusters; j++)
            clusters[i][j] = 0;
    }

    for(int i = 0; i < nInputs; i++)
    {
        int winner = getWinner(i);
        const InputData data = arff->attAt(i, classAtt);
        QString className = data.getSValue();
        int classIndex = classes.indexOf(className);
        clusters[classIndex][winner]++;
    }

    tableClass->setRowCount(classes.size());
    tableMap->setRowCount(height);
    tableMap->setColumnCount(width);

    for(int i = 0; i < classes.size(); i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(classes.at(i));
        item->setBackgroundColor(QColor(Qt::GlobalColor(7+i)));
        tableClass->setItem(i, 0, item);
    }

    if(classes.size() <= 3)
    {
        for(int i = 0; i < width; i++)
            tableMap->setColumnWidth(i, tableMap->rowHeight(1));

        int maxInput[3];

        for(int i = 0; i < classes.size(); i++)
        {
            maxInput[i] = 1;

            for(int j = 0; j < nClusters; j++)
            {
                maxInput[i] = maxInput[i] > clusters[i][j] ? maxInput[i] : clusters[i][j];
            }
        }

        QVector<QColor> colors;
        colors << QColor(255, 0, 0) << QColor(0, 255, 0) << QColor(0, 0, 255);

        int rgb[3] = {55, 55, 55};

        for(int i = 0; i < height; i++)
        {
            for(int j = 0; j < width; j++)
            {
                for(int k = 0; k < classes.size(); k++)
                {
                    rgb[k] = ((200.0f * (float)(clusters[k][i * width + j])) / (float)(maxInput[k])) + 55.0f;
                }

                QTableWidgetItem *item = new QTableWidgetItem();
                item->setBackgroundColor(QColor(rgb[0], rgb[1], rgb[2]));
                tableMap->setItem(i, j, item);

            }
        }
    }
    else
    {
        for(int i = 0; i < height; i++)
        {
            for(int j = 0; j < width; j++)
            {
                int indexClass = -1;
                int counter = 0;
                int nClasses = 0;

                for(int k = 0; k < classes.size(); k++)
                {
                    if(clusters[k][i * width + j] > counter)
                    {
                        indexClass = k;
                        counter = clusters[k][i * width + j];
                    }

                    if(clusters[k][i * width + j] != 0)
                    {
                        nClasses++;
                    }
                }


                if(indexClass != -1)
                {
                    QTableWidgetItem *item = new QTableWidgetItem(classes.at(indexClass) + " (" + QString().setNum(counter) + ")" +
                                                                  (nClasses != 1 ? "*" : QString()) );
                    item->setBackgroundColor(QColor(Qt::GlobalColor(indexClass+7)));
                    tableMap->setItem(i, j, item);
                }
            }
        }
    }

    for(int i = 0; i < classes.size(); i++)
        delete[] clusters[i];
    delete[] clusters;
}

void SOM::init() throw (NetworkException)
{
    textBrowser = new QTextBrowser();
    featureMap = new FeatureMap();

    QString line = "<span style = 'color: red'>Run Information</span><br>";
    textBrowser->append(line);

    try
    {
        readArgs();
        readArff();
    }
    catch(NetworkException e)
    {
        delete featureMap;
        delete textBrowser;
        featureMap = 0;
        textBrowser = 0;
        throw e;
    }

    srand(time(0));

    weight = new double*[nClusters];
    for(int i = 0; i < nClusters; i++)
        weight[i] = new double[nAttributes];

    for(int i = 0; i < nClusters; i++)
    {
        for(int j = 0; j < nAttributes; j++)
        {
            weight[i][j] = (double) rand()/RAND_MAX;
        }
    }
}

void SOM::clear()
{
    for(int i = 0; i < nClusters; i++)
        delete[] weight[i];
    delete[] weight;

    for(int i = 0; i < nInputs; i++)
        delete[] inputs[i];
    delete[] inputs;
}

int SOM::getWinner(int k)
{
    //calculando o vencedor
    int winner = 0;
    double diff = 999999999;

    for(int i = 0; i < nClusters; i++)
    {
        double newdiff = euclidian(inputs[k], weight[i], nAttributes);

        if(newdiff < diff)
        {
            diff = newdiff;
            winner = i;
        }
    }
    return winner;
}

// Funcao da vizinhanca baseado na distancia entre os neuronios i e j
// na epoca t
double SOM::h(int i, int j, int t) throw (NetworkException)
{
    return exp(-distance2(i, j) / (2.0 * pow(sigma(t), 2)));
}

// Funcao de aprendizado na epoca t
double SOM::n(int t)
{
    return initialLearningRate[phase] * exp(- (double)(t) / learningRateK[phase]);
}

// Distancia euclidiana entre os vetores i e j
double SOM::euclidian(double *i, double *j, int size)
{
    double result = 0;
    for(int k = 0; k < size; k++)
        result += ((i[k]-j[k])*(i[k]-j[k]));
    return sqrt(result);
}

// Distancia ao quadrado de dois neuronios no lattice
double SOM::distance2(int i, int j)
{
    double a[2], b[2];

    a[0] = i / width;
    a[1] = i % width;

    b[0] = j / width;
    b[1] = j % width;

    return pow(euclidian(a, b, 2), 2);
}

// Auxiliar da funcao de vizinhanca
double SOM::sigma(int t) throw (NetworkException)
{
    double result = initialEffectiveWidth[phase] * exp(-(double)(t) / effectiveWidthK[phase]);

    if(result < 0.0000000000000000000000000000000000000001)
    {
        throw NetworkException("Division by zero");
    }

    return result;
}
