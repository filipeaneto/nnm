#ifndef CLUSTER_H
#define CLUSTER_H

#include <QWidget>

#include <model.h>
#include <neuralnetwork.h>
#include <som.h>

namespace Ui {
    class Cluster;
}

class Cluster : public QWidget
{
    Q_OBJECT

public:
    explicit Cluster(Model *m, QWidget *parent = 0);
    ~Cluster();

    void setArff();
    void setEnabled(bool);

private slots:
    void on_radioButton_PercentageSplit_toggled(bool checked);

    void on_pushButton_Start_clicked();

    void on_comboBox_Nets_currentIndexChanged(int index);

    void runNetwork();

    void on_comboBox_Results_currentIndexChanged(int index);

private:
    Ui::Cluster *ui;
    Model *model;
    QVector<NeuralNetwork*> nets;

    QVector<QWidget*> resultTabs;
    QVector<QTextBrowser*> resultText;
    QTextBrowser* lastTextBrowser;
};

#endif // CLUSTER_H
