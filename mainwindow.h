#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <utils.h>
#include <preprocess.h>
#include <cluster.h>
#include <classify.h>
#include <model.h>
#include <arffdata.h>
#include <subject.h>
#include <preprocessobserver.h>
#include <clusterobserver.h>
#include <classifyobserver.h>
#include <mainobserver.h>

class MainObserver;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void updateResultWidget();
    void showMessage();

private:
    Ui::MainWindow *ui;

    Preprocess *preprocess;
    Classify *classify;
    Cluster *cluster;

    QWidget *lastResult;
    Model *model;
};

#endif // MAINWINDOW_H
