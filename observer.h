#ifndef OBSERVER_H
#define OBSERVER_H

class Observer
{
public:
    virtual void update(int ID) = 0;
};

#endif // OBSERVER_H
