#ifndef CLASSIFY_H
#define CLASSIFY_H

#include <QWidget>
#include <model.h>
#include <neuralnetwork.h>
#include <hopfield.h>

namespace Ui {
    class Classify;
}

class Classify : public QWidget
{
    Q_OBJECT

public:
    explicit Classify(Model *_model, QWidget *parent = 0);
    ~Classify();

    void setArff(int type);
    void setEnabled(bool);

private slots:
    void on_radioButton_PercentageSplit_toggled(bool checked);

    void on_radioButton_CrossValidation_toggled(bool checked);

    void on_comboBox_Nets_currentIndexChanged(int index);

    void on_pushButton_Start_clicked();

    void on_comboBox_Results_currentIndexChanged(int index);

protected:
    void runNetwork();
    void byTrainingSet(NeuralNetwork *net);
    void byPercentageSplit(NeuralNetwork * net);
    void byCrossValidation(NeuralNetwork * net);

private:
    Ui::Classify *ui;
    Model *model;
    QVector<NeuralNetwork*> nets;

    QVector<QWidget*> resultTabs;
    QVector<QTextBrowser*> resultText;
    QTextBrowser* lastTextBrowser;

    int arffType;
};

#endif // CLASSIFY_H
