#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QDateTime>
#include <QTextBrowser>

class Utils
{
public:
    static QString getNow();
    QTextBrowser &merge(const QTextBrowser &t1, const QTextBrowser &t2);
};

class Notification
{
public:
    const static int None = 0;
    static const int ArffModification = 1;
    static const int ResultModification = 2;
    static const int StatusModification = 4;
    static const int PatternModification = 8;
};

#endif // UTILS_H
