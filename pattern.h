#ifndef PATTERN_H
#define PATTERN_H

#include <arffdata.h>

class Pattern : public ArffData
{
public:
    Pattern(const QString &filename) throw (ArffException);

protected:
    bool readDataLine(const QString &line) throw (ArffException);
    bool readHeaderLine(const QString &line) throw (ArffException);

private:
    int width, height;
};

#endif // PATTERN_H
