#include "preprocess.h"
#include "ui_preprocess.h"

Preprocess::Preprocess(Model *m, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Preprocess),
    model(m),
    selected(0)
{
    ui->setupUi(this);
}

Preprocess::~Preprocess()
{
    delete ui;
}

void Preprocess::on_pushButton_OpenFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, QString(), QString(), "Data file (*.arff *.pat)");

    if(fileName.isEmpty())
        return;

    if(model != 0) {
        model->setArff(fileName);
    }
}

void Preprocess::updateArff()
{
    const ArffData *arff = model->getArff();

    int nAttributes = arff->getNAttributes();

    ui->label_Relation->setText("Relation: " + arff->getRelation());
    ui->label_Instances->setText("Instances: " + QString().setNum(arff->getNInstances()));
    ui->label_Attributes->setText("Attributes: " + QString().setNum(nAttributes));

    selected = 0;
    QList<QRadioButton*>::iterator it = attributes.begin(), tmp;
    while(it != attributes.end())
    {
        tmp = it++;
        ui->scrollAreaWidgetContents_Attributes->layout()->removeWidget(*tmp);
        delete *tmp;
    }
    attributes.clear();

    for(int i = 0; i < nAttributes; i++)
    {
        QString name = QString().setNum(i) + " - " + arff->getAttName(i) + " (" + arff->getAttType(i) + ")";
        QRadioButton *attribute = new QRadioButton(name, this);

        attributes.push_back(attribute);

        connect(attribute, SIGNAL(toggled(bool)), SLOT(onRadioToggled(bool)));

        ui->scrollAreaWidgetContents_Attributes->layout()->addWidget(attribute);
    }
}

void Preprocess::onRadioToggled(bool checked)
{
    if(checked == true)
    {
        selected = qobject_cast< QRadioButton* >(QObject::sender());
    }
}

void Preprocess::on_pushButton_Remove_clicked()
{
    if(selected == 0)
        return;

    model->removeArffAtt(selected->text().left(2).toInt());
}
