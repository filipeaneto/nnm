#include "classify.h"
#include "ui_classify.h"

Classify::Classify(Model *_model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Classify),
    model(_model)
{
    ui->setupUi(this);
    //setEnabled(false);
    ui->pushButton_Start->setEnabled(false);


    nets.push_back(new Hopfield());

    for(int i = 0; i < nets.size(); i++)
    {
        ui->comboBox_Nets->addItem(nets[i]->name());
    }

    lastTextBrowser = ui->textBrowser;
}

Classify::~Classify()
{
    delete ui;
}

void Classify::on_radioButton_PercentageSplit_toggled(bool checked)
{
    ui->doubleSpinBox_PercentageSplit->setEnabled(checked);
}

void Classify::on_radioButton_CrossValidation_toggled(bool checked)
{
    ui->spinBox_Folds->setEnabled(checked);
}

void Classify::setArff(int type)
{
    arffType = type;

    ui->comboBox_Class->clear();

    for(int i = 0; i < nets.size(); i++)
    {
        nets[i]->setArff(model->getArff());
    }

    for(int i = 0; i < model->getArff()->getNAttributes(); i++)
    {
        QString type = model->getArff()->getAttType(i);
        if(type.compare("nominal", Qt::CaseInsensitive) == 0)
        {
            ui->comboBox_Class->addItem(model->getArff()->getAttName(i));
        }
    }


}

void Classify::on_pushButton_Start_clicked()
{
    ui->pushButton_Start->setEnabled(false);

    runNetwork();

    ui->pushButton_Start->setEnabled(true);
}

void Classify::on_comboBox_Nets_currentIndexChanged(int index)
{
    ui->lineEdit_Args->setText(nets[index]->defaultArgs());
}

void Classify::runNetwork()
{
    if(ui->comboBox_Class->currentText().isEmpty())
    {
        model->feedStatus("Relation " + model->getArff()->getRelation() + " is invalid for classification.");
        return;
    }

    NeuralNetwork *net = nets[ui->comboBox_Nets->currentIndex()];
    net->setArgs(ui->lineEdit_Args->text());

    if(!(arffType & net->arffTypeExpected()))
    {
        model->feedStatus("Relation " + model->getArff()->getRelation() + " is invalid for this network.");
        return;
    }

    ui->textBrowser->clear();

    if(ui->radioButton_UseTrainingSet->isChecked())
    {
        byTrainingSet(net);
    }
    else if(ui->radioButton_PercentageSplit->isChecked())
    {
        byPercentageSplit(net);
    }
    else
    {
        byCrossValidation(net);
    }
}

void Classify::on_comboBox_Results_currentIndexChanged(int index)
{
    if(index >= resultTabs.size())
        return;

    model->setResultWidget(resultTabs.at(index));
    resultText.at(index)->setVisible(true);
    lastTextBrowser->setVisible(false);
    lastTextBrowser = resultText.at(index);
}

void Classify::byTrainingSet(NeuralNetwork *net)
{
    try
    {
        net->setClass(ui->comboBox_Class->currentText());

        model->feedStatus(net->name() + " was started using " + model->getArff()->getRelation() + " relation.");

        net->run();

        model->feedStatus(net->name() + " was successful executed.");

        ui->comboBox_Results->addItem(QString().setNum(resultTabs.size()+1) + " - " + net->name()
                                      + " (" + model->getArff()->getRelation() + ")");
        ui->comboBox_Results->setCurrentIndex(resultTabs.size());

        QWidget *widget = net->popWidget();
        QTextBrowser *tb = net->popTextBrowser();

        resultTabs.push_back(widget);
        resultText.push_back(tb);

        model->setResultWidget(widget);
        ui->groupBox_Output->layout()->addWidget(tb);
        lastTextBrowser->setVisible(false);
        lastTextBrowser = tb;
    }
    catch(NetworkException &e)
    {
        model->feedStatus(net->name() + " couldn't be started: " + e.what() + ".");
    }
}

void Classify::byPercentageSplit(NeuralNetwork *net)
{
    try
    {
        net->setClass(ui->comboBox_Class->currentText());

        model->feedStatus(net->name() + " was started using " + model->getArff()->getRelation() + " relation.");

        const ArffData *arff = model->getArff();
        ArffData *a1 = new ArffData();
        ArffData *a2 = new ArffData();
        arff->split(ui->doubleSpinBox_PercentageSplit->value(), a1, a2);

        net->setArff(a1);

        net->run();

        net->setArff(a2);

        model->feedStatus(net->name() + " was successfull executed.");

        ui->comboBox_Results->addItem(QString().setNum(resultTabs.size()+1) + " - " + net->name()
                                      + " (" + model->getArff()->getRelation() + ")");
        ui->comboBox_Results->setCurrentIndex(resultTabs.size());

        QWidget *widget = net->popWidget();
        QTextBrowser *tb = net->popTextBrowser();

        resultTabs.push_back(widget);
        resultText.push_back(tb);

        model->setResultWidget(widget);
        ui->groupBox_Output->layout()->addWidget(tb);
        lastTextBrowser->setVisible(false);
        lastTextBrowser = tb;
    }
    catch(NetworkException &e)
    {
        model->feedStatus(net->name() + " couldn't be started: " + e.what() + ".");
    }
}

void Classify::byCrossValidation(NeuralNetwork *net)
{
    try
    {
        net->setClass(ui->comboBox_Class->currentText());

        model->feedStatus(net->name() + " was started using " + model->getArff()->getRelation() + " relation.");

        const ArffData *arff = model->getArff();
        QList<ArffData *> arffs;
        arff->split(ui->spinBox_Folds->value(), arffs);

        // coloquei a declaracao aqui porque ficou dentro do for
        // acho q vai mudar o esquema do widget entao coloquei ela aqui
        QWidget *widget;
        QTextBrowser *tb;

        for(int i = 0; i < arffs.size(); i++)
        {
            for(int j = 0; j < arffs.size(); j++)
            {
                // quando i e j forem iguais o fold i
                // sera o teste
                if(j != i)
                {
                    net->setArff(arffs.at(j));
                    net->run();
                    // falta fazer a parte de armazenar pra fazer os calculos
                }
            }

            net->setArff(arffs.at(i));

            model->feedStatus(net->name() + " was successful executed.");

            ui->comboBox_Results->addItem(QString().setNum(resultTabs.size()+1) + " - " + net->name()
                                          + " (" + model->getArff()->getRelation() + ")");
            ui->comboBox_Results->setCurrentIndex(resultTabs.size());

            widget = net->popWidget();
            tb = net->popTextBrowser();
        }

        resultTabs.push_back(widget);
        resultText.push_back(tb);

        model->setResultWidget(widget);
        ui->groupBox_Output->layout()->addWidget(tb);
        lastTextBrowser->setVisible(false);
        lastTextBrowser = tb;
    }
    catch(NetworkException &e)
    {
        model->feedStatus(net->name() + " couldn't be started: " + e.what() + ".");
    }
}

void Classify::setEnabled(bool flag) {
    ui->pushButton_Start->setEnabled(flag);
}
