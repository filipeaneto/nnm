#ifndef PREPROCESS_H
#define PREPROCESS_H

#include <QWidget>
#include <QFileDialog>
#include <model.h>
#include <utils.h>
#include <QRadioButton>

namespace Ui {
    class Preprocess;
}

class Preprocess : public QWidget
{
    Q_OBJECT

public:
    explicit Preprocess(Model *m, QWidget *parent = 0);
    ~Preprocess();

    void updateArff();

private slots:
    void on_pushButton_OpenFile_clicked();
    void onRadioToggled(bool checked);

    void on_pushButton_Remove_clicked();

private:
    Ui::Preprocess *ui;
    Model *model;

    QList<QRadioButton*> attributes;
    QRadioButton *selected;
};

#endif // PREPROCESS_H
