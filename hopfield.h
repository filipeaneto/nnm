#ifndef HOPFIELD_H
#define HOPFIELD_H

#include <neuralnetwork.h>
#include <patternview.h>

class Hopfield : public NeuralNetwork
{
public:
    Hopfield();

    // Metodos herdados da classe NeuralNetwork e Classifier
    void setClass(const QString &classAtt);
    void setArgs(const QString &args);
    void setArff(const ArffData *arff);

    QTextBrowser *popTextBrowser() throw (NetworkException);
    QWidget *popWidget() throw (NetworkException);

    void run() throw (NetworkException);

    const QString defaultArgs();
    const QString name();

    int arffTypeExpected() const;

protected:
    void readArgs() throw (NetworkException);
    void readArff() throw (NetworkException);
    void init() throw (NetworkException);
    void clear();
    void fillWidget();

private:
    QTextBrowser *textBrowser;
    PatternView *patternView;
    const ArffData *arff;
    QStringList patterns;
    QString patternAtt;
    QString args;

    QString netDefaultArgs;
    QString netName;

    int width, height;

    double ** weight;
    double *threshold;
    int ** pInputs;
    int ** inputs;
    int nNeurons;
    int nInputs;

};

#endif // HOPFIELD_H
