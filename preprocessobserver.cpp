#include "preprocessobserver.h"

PreprocessObserver::PreprocessObserver(Preprocess *prep) :
    preprocess(prep)
{

}

void PreprocessObserver::update(int ID)
{
    if(ID & (Notification::ArffModification | Notification::PatternModification))
    {
        preprocess->updateArff();
    }
}
