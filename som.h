#ifndef SOM_H
#define SOM_H

#include <QStringList>
#include <QString>

#include <ctime>
#include <math.h>

#include "neuralnetwork.h"
#include "featuremap.h"
#include "arffdata.h"

// Implementacao da rede Self-Organized Map
class SOM : public NeuralNetwork
{

public:
    SOM();
    ~SOM();

    // Metodos herdados da classe NeuralNetwork
    void setClass(const QString &classAtt);
    void setArgs(const QString &args);
    void setArff(const ArffData *arff);

    QTextBrowser *popTextBrowser() throw (NetworkException);
    QWidget *popWidget() throw (NetworkException);

    void run() throw (NetworkException);

    const QString defaultArgs();
    const QString name();

    int arffTypeExpected() const;

protected:
    // Metodos de controle interno
    void readArgs() throw (NetworkException);
    void readArff() throw (NetworkException);
    void init() throw (NetworkException);
    void fillWidget();
    void fillWeightMatrix();
    void fillFeatureMap();
    void clear();

    // Metodos de apoio especificos SOM
    int getWinner(int k);

    double h(int i, int j, int t) throw (NetworkException);
    double n(int t);
    double euclidian(double *i, double *j, int size);
    double distance2(int i, int j);
    double sigma(int t) throw (NetworkException);

    const static int OrderingPhase = 0;
    const static int ConvergencePhase = 1;

private:
    // Atributos de controle interno
    QTextBrowser *textBrowser;
    FeatureMap *featureMap;
    const ArffData *arff;
    QString classAtt;
    QString args;
    
    QString netDefaultArgs;
    QString netName;

    // Atributos especificos do SOM
    double **weight;
    double **inputs;

    int nClusters;
    int nAttributes;
    int nInputs;

    int height;
    int width;

    int phase;

    // Atributos da fase de [0] = ordenacao, [1] = convergencia
    double initialLearningRate[2];
    double learningRateK[2];
    double initialEffectiveWidth[2];
    double effectiveWidthK[2];
    int nEpochs[2];

};

#endif // SOM_H
