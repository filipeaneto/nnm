#include "pattern.h"

Pattern::Pattern(const QString &filename) throw (ArffException)
{
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file);

    bool header = true;

    while(!stream.atEnd()){
        QString line = stream.readLine();

        if(line.isEmpty() || line.startsWith("%"))
            continue;

        header = header ? readHeaderLine(line) : readDataLine(line);
    }
}

bool Pattern::readDataLine(const QString &line) throw (ArffException)
{
    static int lastLine = -1;
    static QStringList allValues;

    lastLine++;


    if(lastLine < height)
    {
        QStringList values;
        for(int i = 0; i < width; i++)
        {
            values.push_back(line.at(i));
        }

        allValues.append(values);
    }
    else if(lastLine == height)
    {
        allValues.append(line.at(0));

        if(allValues.size() != atts.size())
        {
            throw ArffException("Wrong input data size");
        }

        data.push_back(InputVector(atts, allValues));


        lastLine = -1;
        allValues.clear();
    }

    return false;
}

bool Pattern::readHeaderLine(const QString &line) throw (ArffException)
{
    if( line.startsWith("@data", Qt::CaseInsensitive) )
    {
        return false;
    }
    else if( line.startsWith("@class", Qt::CaseInsensitive) )
    {
        QString corrected(line);
        corrected.replace("{"," ");
        corrected.replace("}"," ");
        corrected.replace(","," ");

        // @class attName attType
        const QStringList list = corrected.split(QRegExp("\\s+"), QString::SkipEmptyParts); // separa todas as palavras

        if(list[2].compare("real", Qt::CaseInsensitive) == 0 ||
                list[2].compare("integer", Qt::CaseInsensitive) == 0)
        {
            atts.push_back(Attribute(list[1], list[2]));
        }
        else if(list.size() > 3)
        {
            QStringList type;
            type.push_back("nominal");
            for(int i = 2; i < list.size(); i++)
            {
                type.push_back(list[i]);
            }

            atts.push_back(Attribute("class", type));
        }

        else
        {
            throw ArffException("Attribute not supported");
        }
    }
    else if(line.startsWith("@relation", Qt::CaseInsensitive) )
    {
        // @relation relationName
        relation = line.split(QRegExp("\\s+"))[1]; // separa por espaco
    }
    else if(line.startsWith("@pattern", Qt::CaseInsensitive))
    {
        // @pattern w h
        width = line.split(QRegExp("\\s+"))[1].toInt();
        height = line.split(QRegExp("\\s+"))[2].toInt();

        for(int i = 0; i < width*height; i++)
        {
            atts.push_back(Attribute(QString().setNum(i), "integer"));
        }
    }

    return true;
}
