#include "classifyobserver.h"

ClassifyObserver::ClassifyObserver(Classify *c) :
    classify(c)
{
}

void ClassifyObserver::update(int ID)
{
    if(ID & Notification::PatternModification)
    {
        classify->setEnabled(true);
        classify->setArff(2);
    }
    else if(ID & Notification::ArffModification)
    {
        classify->setEnabled(false);
    }
}
