#ifndef RECEIVER_H
#define RECEIVER_H

#include <arffdata.h>
#include <pattern.h>
#include <observer.h>
#include <subject.h>
#include <utils.h>

class Model : public Subject
{
public:
    Model();
    ~Model();

    void addObserver(Observer *observer);
    void notify();

    void setArff(const QString &filename);
    void setResultWidget(QWidget *widget);
    void removeArffAtt(int i);
    void feedStatus(const QString &status);

    const ArffData *getArff() const;
    QWidget *getResultWidget() const;
    QStringList consumeStatus();

private:
    QList<Observer*> observers;

    ArffData *arff;
    QWidget *result;
    int notificationID;

    QStringList statusList;
};

#endif // RECEIVER_H
