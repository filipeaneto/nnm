#ifndef PREPROCESSOBSERVER_H
#define PREPROCESSOBSERVER_H

#include <observer.h>
#include <preprocess.h>
#include <utils.h>

class PreprocessObserver : public Observer
{
public:
    PreprocessObserver(Preprocess *prep);

    void update(int ID);

private:
    Preprocess *preprocess;

};

#endif // PREPROCESSOBSERVER_H
