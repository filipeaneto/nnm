#ifndef SUBJECT_H
#define SUBJECT_H

class Subject
{
public:
    virtual void addObserver(Observer *observer) = 0;
    virtual void notify() = 0;
};

#endif // SUBJECT_H
