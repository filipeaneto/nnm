#ifndef ARFFDATA_H
#define ARFFDATA_H

#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QRegExp>
#include <exception>
#include <QVector>


class ArffException : public std::exception
{
public:
    ArffException(const char *m) : message(m) {}
    const char *what() const throw () { return message; }

private:
    const char *message;
};

class InputData
{
public:
    InputData();
    InputData(double value, const QString &t = QString("real"));
    InputData(int value, const QString &t = QString("integer"));
    InputData(const QString &value, const QString &t = QString("nominal"));
    InputData(const InputData &source);
    ~InputData();

    const InputData &operator=(const InputData &source);

    void setValue(double value) throw (ArffException);
    void setValue(int value) throw (ArffException);
    void setValue(const QString &value) throw (ArffException);

    double getDValue() const throw (ArffException);
    int getIValue() const throw (ArffException);
    const QString &getSValue() const throw (ArffException);

    const QString &getType() const;

private:
    double dValue;
    int iValue;
    QString sValue;
    QString type;
};

struct Attribute
{
    Attribute(const QString &n, const QString &t) :
        name(n)
    {
        type.push_back(t);
    }

    Attribute(const QString &n, const QStringList &t) :
        name(n), type(t)
    {

    }

    Attribute(const Attribute &source) :
        name(source.name), type(source.type)
    {

    }

    const Attribute &operator=(const Attribute &source)
    {
        name = source.name;
        type = source.type;
        return *this;
    }

    QString name;
    QStringList type;
};

class InputVector
{
public:
    InputVector(const QList<Attribute> &t, const QStringList &values) throw (ArffException);
    InputVector(const InputVector &source);
    ~InputVector();

    const InputData &get(int i) const;

    void removeAttribute(int i);

    const InputVector &operator =(const InputVector &source);
private:
    QStringList dataType;
    QList<InputData> inputs;
};

class ArffData
{
public:
    ArffData(QString fileName) throw (ArffException);
    ArffData() throw (ArffException);
    ArffData(const QList<ArffData *> &sources) throw (ArffException);
    virtual ~ArffData();

    const InputData &at(int i, int j) const;

    const QString &getRelation() const;
    const QString &getAttName(int i) const;
    const QString &getAttType(int i) const;

    int getNInstances() const;
    int getNAttributes() const;

    void removeAttribute(int i);

    int countNumericAttributes() const;

    QStringList classesFrom(const QString &attributeName) const;
    const InputData &attAt(int i, const QString &attName) const;

    void split(float percent, ArffData *a1, ArffData *a2) const throw (ArffException);
    void split(int fold, QList<ArffData *> &arffs) const throw (ArffException);

protected:
    virtual bool readDataLine(const QString &line) throw (ArffException);
    virtual bool readHeaderLine(const QString &line) throw (ArffException);

    QString relation;
    QList<InputVector> data;
    QList<Attribute> atts;
};

#endif // ARFFDATA_H
