#include "patternview.h"
#include "ui_patternview.h"

PatternView::PatternView(int _width, int _height, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PatternView),
    weight(0),
    threshold(0),
    pInputs(0),
    inputs(0),
    width(_width),
    height(_height)
{
    ui->setupUi(this);
}

PatternView::~PatternView()
{
    delete ui;
}

void PatternView::setWeightMatrix(double **weight)
{
    this->weight = weight;
}

void PatternView::setPatterns(int **pInputs, const QStringList &patterns)
{
    this->pInputs = pInputs;
    this->patterns = patterns;
}

void PatternView::setInputs(int **inputs, int n)
{
    nInputs = n;
    this->inputs = inputs;
}

void PatternView::setThreshold(double *threshold)
{
    this->threshold = threshold;
}

void PatternView::init()
{
        ui->spinBox_Training->setMinimum(0);
        ui->spinBox_Training->setMaximum(patterns.size()-1);
        ui->spinBox_Input->setMinimum(0);
        ui->spinBox_Input->setMaximum(nInputs-1);
        on_spinBox_Training_valueChanged(0);
        on_spinBox_Input_valueChanged(0);
}

void PatternView::on_spinBox_Training_valueChanged(int arg1)
{
    if(!(weight && pInputs && inputs))
        return;

    ui->label_PatternClass->setText(patterns.at(arg1));

    ui->tableWidget_Training->clear();
    ui->tableWidget_Training->setColumnCount(width);
    ui->tableWidget_Training->setRowCount(height);

    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            QTableWidgetItem *widget = new QTableWidgetItem();

            if(pInputs[arg1][i*width+j] > 0)
                widget->setBackgroundColor(Qt::black);
            else
                widget->setBackgroundColor(Qt::white);

            ui->tableWidget_Training->setItem(i, j, widget);
        }
    }
}

void PatternView::on_spinBox_Input_valueChanged(int arg1)
{
    if(!(weight && pInputs && inputs))
        return;

    ui->tableWidget_After->clear();
    ui->tableWidget_Before->clear();
    ui->tableWidget_Before->setColumnCount(width);
    ui->tableWidget_Before->setRowCount(height);


    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            QTableWidgetItem *widget = new QTableWidgetItem();

            if(inputs[arg1][i*width+j] == 1)
                widget->setBackgroundColor(Qt::black);
            else
                widget->setBackgroundColor(Qt::white);

            ui->tableWidget_Before->setItem(i, j, widget);
        }
    }

    ui->label_Info->clear();
}

void PatternView::on_pushButton_Start_clicked()
{
    if(!(weight && pInputs && inputs))
        return;

    ui->tableWidget_After->clear();
    ui->tableWidget_After->setColumnCount(width);
    ui->tableWidget_After->setRowCount(height);

    int size = width*height;
    int input = ui->spinBox_Input->value();
    int *result = new int[size];
    double aux;

    


    for(int i = 0; i < size; i++)
    {
        aux = 0;
        for(int j = 0; j < size; j++)
        {
            aux += weight[i][j] * inputs[input][j];
        }
        aux -= threshold[i];
        result[i] = signal(aux, inputs[input][i]);
    }

    bool run = true;
    int *newresult = new int[size];

    for(int k = 0; k < 1000 && run; k++)
    {
        // Rodando a rede de novo
        for(int i = 0; i < size; i++)
        {
            aux = 0;
            for(int j = 0; j < size; j++)
            {
                aux += weight[i][j] * result[j];
            }
            aux -= threshold[i];
            newresult[i] = signal(aux, result[i]);
        }

        run = false;

        for(int i = 0; i < size; i++)
        {
            if(newresult[i] != result[i])
            {
                run = true;
            }
            result[i] = newresult[i];
        }
    }

    delete[] newresult;

    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            QTableWidgetItem *widget = new QTableWidgetItem();

            if(result[i*width+j] == 1)
                widget->setBackgroundColor(Qt::black);
            else
                widget->setBackgroundColor(Qt::white);

            ui->tableWidget_After->setItem(i, j, widget);
        }
    }

    int hDistance = size+1;
    int similar = 0;

    for(int i = 0; i < patterns.size(); i++)
    {
        int tmp = 0;
        for(int j = 0; j < size; j++)
        {
            if(result[j] != pInputs[i][j])
            {
                tmp++;
            }
        }

        if(tmp < hDistance)
        {
            hDistance = tmp;
            similar = i;
        }
    }

    ui->label_Info->setText("Similar to " + patterns.at(similar) + " with distance " + QString().setNum(hDistance));

    delete[] result;
}

int PatternView::signal(double value, int before)
{
    return value < 0 ? -1 : (value > 0 ? 1 : before);
}

