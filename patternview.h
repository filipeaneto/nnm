#ifndef PATTERNVIEW_H
#define PATTERNVIEW_H

#include <QWidget>

namespace Ui {
class PatternView;
}

class PatternView : public QWidget
{
    Q_OBJECT
    
public:
    explicit PatternView(int _width, int _height, QWidget *parent = 0);
    ~PatternView();
    
    void setWeightMatrix(double ** weight);
    void setPatterns(int ** pInputs, const QStringList &patterns);
    void setInputs(int ** inputs, int n);
    void setThreshold(double *threshold);
    void init();

private slots:
    void on_spinBox_Training_valueChanged(int arg1);

    void on_spinBox_Input_valueChanged(int arg1);

    void on_pushButton_Start_clicked();

protected:
    int signal(double value, int before);

private:
    Ui::PatternView *ui;

    QStringList patterns;

    double ** weight;
    double *threshold;
    int ** pInputs;
    int ** inputs;

    int width, height;
    int nInputs;
};

#endif // PATTERNVIEW_H
